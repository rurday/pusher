<?php

namespace App\Events;

use App\Message;
use App\User;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
        public $user;

    /**
     * @var Message
     */
    public $message;


    /**
     * MessageSent constructor.
     * @param User $user
     * @param Message $message
     */

    public function __construct(User $user , Message $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Creo un canal privado llamado chat Usamos canal privado ya que se necesita autenticacion
     *
     * @return \Illuminate\Broadcasting\Channel|\Illuminate\Broadcasting\Channel[]|PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat');
    }
}
