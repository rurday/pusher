<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatsController extends Controller
{


    /**
     *  Todos los metodos del controlador solo seran accesibles
     *  por usuarios autorizados
     *
     * ChatsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *Muestra el chat
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view("chat");
    }


    /**
     *  Recupera los mensajes
     *
     * @return mixed
     */
    public function fetchMessages()
    {
        return Message::with('user')->get();
    }


    /**
     * Guarda los mensajes en la base de datos y retorna un mensaje status
     *
     * @param Request $request
     * @return array
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => $request->input('message')
        ]);

        //Hago broadcast a nuestro evento de mensaje
            broadcast(new MessageSent($user, $message))->toOthers();

        return ['status' => 'Message Sent!'];

    }
}
