<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{


    /**
     * @var array
     */

    protected $fillable = ['message'];


    /**
     * A message belongsTo a User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class );
    }
}
